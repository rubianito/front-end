import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CustomerService } from '../../services/customer.service';
import { Customer } from '../../models/customer';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
  providers: [CustomerService],
})
export class CustomersComponent implements OnInit {
  validationsForm: any;
  customer: Customer = {
    customerId: 0,
    customerName: '',
    customerLastName: '',
    documentNumber: '',
    phoneNumber: '',
  };

  constructor(public customerService: CustomerService) {}

  ngOnInit(): void {
    this.validationsForm = new FormGroup({
      customerName: new FormControl(),
      customerLastName: new FormControl(),
      phoneNumber: new FormControl(),
      documentNumber: new FormControl(),
    });
  }

  saveCustomer(value: any) {
    if (this.customer.customerId > 0) {
      let customer: Customer = {
        customerId: this.customer.customerId,
        customerName:
          value.customerName == null
            ? this.customer.customerName
            : value.customerName,
        customerLastName:
          value.customerLastName == null
            ? this.customer.customerLastName
            : value.customerLastName,
        documentNumber:
          value.documentNumber == null
            ? this.customer.documentNumber
            : value.documentNumber,
        phoneNumber:
          value.phoneNumber == null
            ? this.customer.phoneNumber
            : value.phoneNumber,
      };
      this.customerService
        .editCustomer(this.customer.customerId, customer)
        .subscribe((data) => {
          if (data.error) {
            alert('Se presentó un error al guardar el cliente');
          } else {
            window.location.reload();
          }
        });
    } else {
      let customer: Customer = {
        customerId: 0,
        customerName: value.customerName,
        customerLastName: value.customerLastName,
        documentNumber: value.documentNumber,
        phoneNumber: value.phoneNumber,
      };
      this.customerService.createCustomer(customer).subscribe((data) => {
        if (data.error) {
          alert('Se presentó un error al guardar el cliente');
        } else {
          window.location.reload();
        }
      });
    }
  }

  getCustomerById(idCustomer: number) {
    this.customerService.getCustomerById(idCustomer).subscribe((data) => {
      if (!data.error) {
        this.customer = {
          customerId: data.result.customerId,
          customerName: data.result.customerName,
          customerLastName: data.result.customerLastName,
          documentNumber: data.result.documentNumber,
          phoneNumber: data.result.phoneNumber,
        };
      }
    });
  }
}
