export interface Customer {
  customerId: number;
  customerName: string;
  customerLastName: string;
  documentNumber: string;
  phoneNumber: string;
}
