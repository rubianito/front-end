import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from '../models/customer';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  constructor(private http: HttpClient) {}

  createCustomer(customer: Customer): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Access-Control-Allow-Origin': '*',
      // 'Access-Control-Allow-Headers': 'Content-Type',
      // 'Access-Control-Allow-Methods': '*',
    });
    return this.http.post(
      'https://localhost:44359/api/Customer/CreateCustomer',
      customer,
      { headers }
    );
  }

  editCustomer(idCustomer: number, customer: Customer): Observable<any> {
    return this.http.put(
      'https://localhost:44359/api/Customer/UpdateCustomer?idCustomer=' +
        idCustomer,
      customer
    );
  }

  deleteCustomer(idCustomer: number): Observable<any> {
    return this.http.post(
      'https://localhost:44359/api/Customer/DeleteCustomer?idCustomer=' +
        idCustomer,
      null
    );
  }

  getCustomers(): Observable<any> {
    return this.http.get('https://localhost:44359/api/Customer/GetCustomers');
  }

  getCustomerById(idCustomer: number): Observable<any> {
    return this.http.get(
      'https://localhost:44359/api/Customer/GetCustomerById?idCustomer=' + idCustomer
    );
  }

  getCustomerByDocument(documentNumber: string): Observable<any> {
    return this.http.get(
      'https://localhost:44359/api/Customer/GetCustomerByDocument?documentNumber=' +
        documentNumber
    );
  }
}
