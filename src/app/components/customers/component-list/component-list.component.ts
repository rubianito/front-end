import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Customer } from 'src/app/models/customer';
import { CustomerService } from '../../../services/customer.service';

@Component({
  selector: 'app-component-list',
  templateUrl: './component-list.component.html',
  styleUrls: ['./component-list.component.scss'],
})
export class ComponentListComponent implements OnInit {
  customers: Customer[] = [];
  @Output() newItemEvent = new EventEmitter<number>();

  constructor(public customerService: CustomerService) {}

  ngOnInit(): void {
    this.getCustomers();
  }

  getCustomers() {
    this.customerService.getCustomers().subscribe((data) => {
      console.log(data);
      if (!data.error) {
        this.customers = data.result;
      }
    });
  }

  deleteCustomer(idCustomer: number) {
    this.customerService.deleteCustomer(idCustomer).subscribe((data) => {
      if (!data.error) {
        this.getCustomers();
      } else {
        alert('Error eliminando registro');
      }
    });
  }

  updateCustomer(idCustomer: number) {
    this.newItemEvent.emit(idCustomer);
  }

  addSale(idCustomer: number) {
    // this.newItemEvent.emit(idCustomer);
  }
}
