import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Product } from '../../models/products';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  validationsForm: any;
  product: Product = {
    productId: 0,
    productName: '',
    productValue: 0,
  };

  constructor(public productService: ProductService) {}

  ngOnInit(): void {
    this.validationsForm = new FormGroup({
      productName: new FormControl(),
      productValue: new FormControl(),
    });
  }

  saveProduct(value: any) {
    if (this.product.productId > 0) {
      let product: Product = {
        productId: this.product.productId,
        productName:
          value.productName == null
            ? this.product.productName
            : value.productName,
        productValue:
          value.productValue == null
            ? this.product.productValue
            : value.productValue,
      };
      this.productService
        .editProduct(this.product.productId, product)
        .subscribe((data) => {
          if (data.error) {
            alert('Se presentó un error al guardar el producto');
          } else {
            window.location.reload();
          }
        });
    } else {
      let product: Product = {
        productId: 0,
        productName: value.productName,
        productValue: value.productValue,
      };
      this.productService.createProduct(product).subscribe((data) => {
        if (data.error) {
          alert('Se presentó un error al guardar el producto');
        } else {
          window.location.reload();
        }
      });
    }
  }

  getProductById(idProduct: number) {
    this.productService.getProductById(idProduct).subscribe((data) => {
      if (!data.error) {
        this.product = {
          productId: data.result.productId,
          productName: data.result.productName,
          productValue: data.result.productValue,
        };
      }
    });
  }
}
