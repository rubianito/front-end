import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Product } from 'src/app/models/products';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  products: Product[] = [];
  @Output() newItemEvent = new EventEmitter<number>();

  constructor(public productService: ProductService) {}

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.productService.getProducts().subscribe((data) => {
      console.log(data);
      if (!data.error) {
        this.products = data.result;
      }
    });
  }

  deleteProduct(idProduct: number) {
    this.productService.deleteProduct(idProduct).subscribe((data) => {
      if (!data.error) {
        this.getProducts();
      } else {
        alert('Error eliminando registro');
      }
    });
  }

  updateProduct(idProduct: number) {
    this.newItemEvent.emit(idProduct);
  }
}
