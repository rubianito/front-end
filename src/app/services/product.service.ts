import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/products';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private http: HttpClient) {}

  createProduct(product: Product): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post(
      'https://localhost:44359/api/Product/CreateProduct',
      product,
      { headers }
    );
  }

  editProduct(idProduct: number, product: Product): Observable<any> {
    return this.http.put(
      'https://localhost:44359/api/Product/UpdateProduct?idProduct=' +
        idProduct,
      product
    );
  }

  deleteProduct(idProduct: number): Observable<any> {
    return this.http.post(
      'https://localhost:44359/api/Product/DeleteProduct?idProduct=' +
        idProduct,
      null
    );
  }

  getProducts(): Observable<any> {
    return this.http.get('https://localhost:44359/api/Product/GetProducts');
  }

  getProductById(idProduct: number): Observable<any> {
    return this.http.get(
      'https://localhost:44359/api/Product/GetProductById?idProduct=' +
        idProduct
    );
  }

  getProductByName(productName: string): Observable<any> {
    return this.http.get(
      'https://localhost:44359/api/Product/GetProductByName?documentNumber=' +
        productName
    );
  }
}
