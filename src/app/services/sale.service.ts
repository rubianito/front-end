import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Sale } from '../models/sale';

@Injectable({
  providedIn: 'root',
})
export class SaleService {
  constructor(private http: HttpClient) {}

  createSale(sale: Sale): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.http.post('https://localhost:44359/api/Sale/CreateSale', sale, {
      headers,
    });
  }

  editSale(idSale: number, sale: Sale): Observable<any> {
    return this.http.put(
      'https://localhost:44359/api/Sale/UpdateSale?idSale=' + idSale,
      sale
    );
  }

  deleteSale(idSale: number): Observable<any> {
    return this.http.post(
      'https://localhost:44359/api/Sale/DeleteSale?idSale=' + idSale,
      null
    );
  }

  getSales(): Observable<any> {
    return this.http.get('https://localhost:44359/api/Sale/GetSales');
  }

  getSaleById(idSale: number): Observable<any> {
    return this.http.get(
      'https://localhost:44359/api/Sale/GetSaleById?idSale=' + idSale
    );
  }

  getSaleByCustomerId(customerId: number): Observable<any> {
    return this.http.get(
      'https://localhost:44359/api/Sale/GetSaleByCustomerId?customerId=' +
        customerId
    );
  }
}
