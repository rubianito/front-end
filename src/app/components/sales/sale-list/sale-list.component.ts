import { Component, Input, OnInit } from '@angular/core';
import { saleProducts } from 'src/app/models/saleProducts';
import { SaleService } from 'src/app/services/sale.service';

@Component({
  selector: 'app-sale-list',
  templateUrl: './sale-list.component.html',
  styleUrls: ['./sale-list.component.scss'],
})
export class SaleListComponent implements OnInit {
  sales: saleProducts[] = [];
  @Input() customerId = 0;

  constructor(public saleService: SaleService) {}

  ngOnInit(): void {
    this.getSales();
  }

  getSales() {
    debugger;
    this.saleService.getSaleByCustomerId(this.customerId).subscribe((data) => {
      console.log(data);
      if (!data.error) {
        this.sales = data.result;
      }
    });
  }

  deleteSale(idSale: number) {
    this.saleService.deleteSale(idSale).subscribe((data) => {
      if (!data.error) {
        this.getSales();
      } else {
        alert('Error eliminando registro');
      }
    });
  }
}
