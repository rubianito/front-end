import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomersComponent } from './components/customers/customers.component';
import { ProductsComponent } from './components/products/products.component';
import { SaleComponent } from './components/sales/sale.component';

const routes: Routes = [
  { path: '', component: CustomersComponent },
  { path: 'product', component: ProductsComponent },
  { path: 'customer', component: CustomersComponent },
  { path: 'sales/:customerId', component: SaleComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
