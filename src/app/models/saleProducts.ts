export interface saleProducts {
  saleId: number;
  productName: string;
  productValue: number;
  quantity: number;
}
