import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Product } from 'src/app/models/products';
import { Sale } from 'src/app/models/sale';
import { ProductService } from 'src/app/services/product.service';
import { SaleService } from 'src/app/services/sale.service';
import { Customer } from '../../models/customer';
import { CustomerService } from '../../services/customer.service';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.scss'],
})
export class SaleComponent implements OnInit {
  validationsForm: any;
  products: Product[] = [];
  customerId = 0;
  customer: Customer = {
    customerId: 0,
    customerLastName: '',
    customerName: '',
    documentNumber: '',
    phoneNumber: '',
  };
  sale: Sale = {
    saleId: 0,
    productId: 0,
    customerId: 0,
    quantity: 0,
  };

  constructor(
    public saleService: SaleService,
    public productService: ProductService,
    public customerService: CustomerService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.validationsForm = new FormGroup({
      productId: new FormControl(),
      quantity: new FormControl(),
    });
    this.getProducts();
    this.customerId = this.activatedRoute.snapshot.params.customerId;
    this.getCustomer();
  }

  getProducts() {
    this.productService.getProducts().subscribe((data) => {
      console.log(data);
      if (!data.error) {
        this.products = data.result;
      }
    });
  }

  getCustomer() {
    this.customerService.getCustomerById(this.customerId).subscribe((data) => {
      this.customer = data.result;
    });
  }

  saveSale(value: any) {
    debugger;
    let sale: Sale = {
      saleId: 0,
      productId: value.productId,
      customerId: this.customerId,
      quantity: value.quantity,
    };
    this.saleService.createSale(sale).subscribe((data) => {
      if (data.error) {
        alert('Se presentó un error al guardar la venta');
      } else {
        window.location.reload();
      }
    });
  }
}
