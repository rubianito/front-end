export interface Sale {
  saleId: number;
  productId: number;
  customerId: number;
  quantity: number;
}
